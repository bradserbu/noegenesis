#!/usr/bin/env node
'use strict';

const Queue = require('../lib/jobs/Queue');

function CLI() {

    const _ = require('lodash');
    const yargs = require('yargs');
    const FS = require('fs-extra');
    const logger = require('nodus-framework').logging.createLogger('noe:run');
    const Files = require('nodus-framework').files;
    const Path = require('path');
    const Promise = require('bluebird');
    const argv = yargs.argv;

    const target = argv._.shift();
    const args = argv._.slice();
    const options = _.omit(argv, '_');

    function Job() {
        const filename = Files.resolvePath(target);
        const dirname = Path.dirname(filename);
        const json = FS.readJsonSync(target);

        const {script} = json;
        const ScriptPath = script => Path.isAbsolute(script) ? script : Path.join(dirname, script);

        return {
            __filename,
            __dirname,
            ...json,
            get script() {
                return ScriptPath(script);
            }
        };
    }

    function Worker() {

        const job = Job();
        const name = job.name;
        const script = job.script;
        const handler = Files.requireFile(script);

        logger.debug('worker:script', script);
        logger.debug('worker:handler', handler);

        Queue.process(name, (job, done) => {
            logger.debug(`jobs:process:job`, job);

            const id = job.id;
            logger.debug(`jobs:process:id`, id);

            const data = job.data;
            logger.debug(`jobs:process:data`, data);

            // const name = job.name;
            // const id = job.id;
            // const data = job.data;
            const args = data.args;
            logger.debug(`jobs:process:args`, args);

            const context = {
                id,
                args
            };

            return Promise.try(() => handler.apply(context, args))
                .then(result => {
                    logger.info('jobs:process:result', result);
                    done(null, result);
                })
                .catch(err => {
                    logger.error('jobs:process:error', err);
                    done(err);
                });
        });
    }

    return Worker();
}

const cli = CLI();
// console.log(cli);

// module.exports = CLI();

// var kue = require('kue')
//     , queue = kue.createQueue();
// const queue = require('../lib/jobs/Queue');
// console.log('queue', queue);
//
// queue.process('echo', function(job, done){
//     email(job.data.to, done);
    // console.log('ECHO', job);
    // const data = job.data;
    //
    // console.log('DATA', data);
    // done(data);
// });
//
// queue.inactiveCount( function( err, total ) { // others are activeCount, completeCount, failedCount, delayedCount
//     console.log('INACTIVE', {total});
//     if( total > 100000 ) {
//         console.log( 'We need some back pressure here' );
//     }
// });