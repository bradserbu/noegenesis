#!/usr/bin/env node
'use strict';

function CLI() {

    const _ = require('lodash');
    const yargs = require('yargs');
    const FS = require('fs-extra');
    const logger = require('nodus-framework').logging.createLogger('noe:run');
    const Program = require('nodus-framework').program;
    const Files = require('nodus-framework').files;
    const Path = require('path');
    const Promise = require('bluebird');
    const argv = yargs.argv;
    const Queue = require('../lib/jobs/Queue');
    const Job = require('../lib/jobs/Job');

    const target = argv._.shift();
    const args = argv._.slice();
    const options = _.omit(argv, '_');

    const PRINT = result => console.log(result);
    const PRINT_ERROR = error => console.log('ERROR:', error);
    const SHUTDOWN = () => Program.shutdown();

    function Submit() {
        const name = target;
        const JOB = Job(name);

        logger.debug('SUBMIT', {name, JOB});

        const job = JOB.submit(args);
        logger.debug('job', job);

        job.on('enqueue', job => logger.debug('ENQUEUE', job));
        job.on('start', job => logger.debug('START', job));
        job.on('complete', result => {
            logger.info('COMPLETE', result);

            PRINT(result);
            SHUTDOWN();
        });
        job.on('error', error => {
            logger.error('ERROR', err);

            PRINT_ERROR(error);
            SHUTDOWN();
        });
    }

    return Submit();
}

const cli = CLI();
// console.log(cli);

// module.exports = CLI();

// var kue = require('kue')
//     , queue = kue.createQueue();
// const queue = require('../lib/jobs/Queue');
// console.log('queue', queue);
//
// queue.process('echo', function(job, done){
//     email(job.data.to, done);
// console.log('ECHO', job);
// const data = job.data;
//
// console.log('DATA', data);
// done(data);
// });
//
// queue.inactiveCount( function( err, total ) { // others are activeCount, completeCount, failedCount, delayedCount
//     console.log('INACTIVE', {total});
//     if( total > 100000 ) {
//         console.log( 'We need some back pressure here' );
//     }
// });