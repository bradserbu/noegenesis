module.exports = () => {
    queue = require('kue').createQueue();

    queue.testMode.enter();

//        queue.testMode.clear();

//        queue.testMode.exit()


    queue.createJob('myJob', {foo: 'bar'}).save();
    queue.createJob('anotherJob', {baz: 'bip'}).save();
    expect(queue.testMode.jobs.length).to.equal(2);
    expect(queue.testMode.jobs[0].type).to.equal('myJob');
    expect(queue.testMode.jobs[0].data).to.eql({foo: 'bar'});

};