module.exports = (message = 'Hello World!') => {

    const Job = require('../../lib/jobs/Job');
    const FS = require('fs-extra');

    const ECHO = Job('echo');

    return new Promise((resolve, reject) => {
        const job = ECHO.submit({message});

        job.on('complete', resolve);
        job.on('error', reject);
    });
};