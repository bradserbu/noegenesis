'use strict';

function Echo(...args) {

    const logger = require('nodus-framework').logging.createLogger();
    logger.debug('ARGS', args);

    const Exec = require('execa');
    const Path = require('path');

    const DEFAULT_SHELL = '/bin/bash';
    const SHELL = process.env['SHELL'] || DEFAULT_SHELL;
    logger.debug('SHELL', SHELL);

    const ECHO_SCRIPT = Path.join(__dirname, 'echo.sh');
    logger.debug('ECHO_SCRIPT', ECHO_SCRIPT);

    const CWD = process.cwd();
    const ARGS = args.map(arg => JSON.stringify(arg));

    logger.debug('THIS', this);
    return Exec(SHELL, [ECHO_SCRIPT, ...ARGS], {
        cwd: CWD
    })
        .then(result => {
            logger.debug('RESULT', result);
            return result.stdout;
        });
}

// ** Exports
module.exports = Echo;