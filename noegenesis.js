'use strict';

// ** NODEJS Bindings
const Require = require;
const WorkerProcess = process;

function NoeGenesis() {

    const Path = Require('path');
    const FS = Require('fs-extra');
    const CWD = Process.cwd();
    const NOE_HOME = Path.join(CWD, '.noe');

    function Init() {
        return Run(FS.ensureDir, NOE_HOME);
    }

    return Run({
        init: Init
    });
}