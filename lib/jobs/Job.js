'use strict';

function Task(task, options) {

}

function Job(name, options = {}) {

    const queue = require('./Queue');

    function submit(args, options) {
        const job = queue.create(name, {args, options}).save();
        return job;
    }

    return {
        name,
        options,
        queue,
        submit
    };
}

// ** Exports
module.exports = Job;