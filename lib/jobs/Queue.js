'use strict';

function Queue() {
    const Config = require('./Config');

    const connection = {
        redis: Config('REDIS_URL')
    };

    const program = require('nodus-framework').program;
    const kue = require('kue');
    const logger = require('nodus-framework').logging.createLogger();
    const queue = kue
        .createQueue()
        .on('job enqueue', function (id, type) {
            logger.debug('job:enqueue', {id, type});
        })
        .on('job complete', function (id, result) {
            logger.deub(`job:complete`, {id, result});
        });

    program.on('shutdown', () => {
        // console.error(queue.client);
        // queue.client.exit()
        queue.shutdown(5000, function (err) {
            logger.debug('Kue shutdown: ', err || '');
            process.exit(0);
        });
    });

    return queue;
}

module.exports = Queue();