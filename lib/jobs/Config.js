'use strict';

function Config() {

    const DEFAULT_CONFIG = {
        REDIS_URL: "redis://nodus-server:6379"
    };

    return DEFAULT_CONFIG;
}

module.exports = Config;