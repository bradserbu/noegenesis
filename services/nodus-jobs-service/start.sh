#!/usr/bin/env bash

#source /NODUS/env

CWD=$PWD
SCRIPT_DIR=$( cd "$(dirname "$0")" ; pwd -P )

cd $SCRIPT_DIR
docker-compose up -d