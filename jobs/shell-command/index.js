'use strict';

function ShellCommand(cmd, options) {

    const Exec = require('execa');

    return (args, options) => {
        const proc = Exec('bash', [cmd, ...args]);
        return proc;
    }
}

// ** Exports
module.exports = ShellCommand;