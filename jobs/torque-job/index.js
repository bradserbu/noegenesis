'use strict';

function TorqueJob (options) {

    function Run(args) {

        const job = queue.create('torque-job', args);

        job.on('complete', function(result){
            console.log('Job completed with data ', result);
        }).on('failed attempt', function(errorMessage, doneAttempts){
            console.log('Job failed');
        }).on('failed', function(errorMessage){
            console.log('Job failed');
        }).on('progress', function(progress, data){
            console.log('\r  job #' + job.id + ' ' + progress + '% complete with data ', data );
        });

        return job;
    }


}

// ** Exports
module.exports = TorqueJob;